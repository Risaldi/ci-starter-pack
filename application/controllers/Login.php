<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

  public function __construct() {
    parent::__construct();

    $this->load->model('project_m');
    $this->load->helper('url');
    // load model project_m dan helper URL, kalian bisa menambahkannya di auto loader agar tidak perlu memanggilnya disetiap class
  }

  public function index() {
    $this->load->view('layouts/header');
    $this->load->view('login/index');
    $this->load->view('layouts/footer');
  }

  /**
  * Fungsi login
  */
  public function doLogin() {
    $where  = array(
      'username'  =>  $this->input->post('username'),
      'password'  =>  md5($this->input->post('password'))
    );

    // print_r($where);

    $data = $this->project_m->get_data('users', $where)->row();
    if ($data) {

      $sessdata['id'] = $data->id;
      $sessdata['name'] = $data->name;
      $sessdata['email']  = $data->email;
      $sessdata['logged_in']  = true;
      $this->session->set_userdata($sessdata);


      // print_r($this->session->userdata());
      redirect(base_url('welcome'));
    }

    return "Gagal";
  }

  function doLogout() {
    $this->session->sess_destroy();
    redirect(base_url('welcome'));
  }

}
