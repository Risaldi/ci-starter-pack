<?php
// File ini yg akan menjadi pusat dari aplikasi kalian.
// Disini kalian bisa menjalankan logikka logika (fungsi)
// Seperti:
// Memanggil view, Komunikasi dengan Model, dll

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{

  /**
  * Contruct adalah fungsi yg otomatis dijalankan pertama
  * Saat sebuah class dijalankan
  */
  public function __construct() {
    // Kalian bisa cari tau sendiri apa maksud parent::__construct, jangan lupa share ke grup jika sudah tau
    parent::__construct();
    // echo "ini bagian contruct";
    // Hapus '//' jika ingin melihat apa maksud dari fungsi yg otomatis dijalankan pertama
    $this->load->model('project_m');
    $this->load->helper('url');
    $this->load->helper('form');
    $this->load->library('form_validation'); // Load library form_validation
    // load model project_m dan helper URL, kalian bisa menambahkannya di auto loader agar tidak perlu memanggilnya disetiap class

    if ( ! $this->session->userdata('logged_in')) {
      redirect(base_url('login'));
    }
    // Cek user sudah login atau belum
  }

  /**
  * Fungsi yg akan menampilkan data dari class ini
  * Fungsi ini akan mem-passing data yg didapat dari model (get_data) ke view
  */
  public function index() {
    $data['users'] =  $this->project_m->get_data('users')->result();
    // Memanggil data dari database dan mengubahnya menjadi associative array

    $this->load->view('layouts/header');
    $this->load->view('users/index', $data);
    $this->load->view('layouts/footer');
    // Mempassing(mengirim) data users ke view agar bisa ditampilkan
    // $this->load->view() seperti include pada php native
  }

  /**
  * Fungsi yg akan menampilkan form input
  */
  public function create() {
    $data['title']  = "Tambah";
    // Buat Form Validation
    $this->form_validation->set_rules('username', 'Username', 'trim');
    $this->form_validation->set_rules('password', 'Password', 'trim|required');
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

    if ($this->form_validation->run() == FALSE) {
      $this->load->view('layouts/header');
      $this->load->view('users/create', $data);
      $this->load->view('layouts/footer');
      // Menampilkan form input data

    } else {
      $this->store();
      // Memanggil fungsi untuk simpan ke database
    }

  }

  /**
  * Fungsi yg akan menyimpan data dari form input ke tabel
  */
  public function store() {
    $path     = './assets/photo/users/';
    $doUpload = $this->project_m->upload_foto('photo', $path);
    // print_r($doUpload);
    // Hapus komentar untuk melihat data dari foto yg diupload

    // $this->input->post() == $_POST['']
    // Ada 2 cara untuk menyimpan data yg akan dimasukan ke database
    // Cara 1.
    // Syarat: Name pada form input harus sama dengan yg ada di tabel pada database
    // Kelebihan: Lebih cepat dan efisien
    // $data = $this->input->post();

    // Cara 2.
    // Syarat: Data harus dalam bentuk array
    // Kelebihan: Lebih leluasa untuk mengedit valuenya
    $name_escape     = $this->db->escape_str($this->input->post('name'));
    $username_escape = $this->db->escape_str($this->input->post('username'));
    $email_escape    = $this->db->escape_str($this->input->post('email'));
    // escape_str, Escape string berarti mengurangi ambiguitas dalam tanda kutip (dan karakter lain) yang digunakan dalam string itu.
    // Misalnya, input bervalue <script>alert('Hi')</script> akan menjadi <script>alert(\'Hi\')</script>
    // Dan xss clean akan merubahnya lagi menjadi [removed]alert(\'Hi\')[removed]
    // Selebihnya: https://stackoverflow.com/questions/10646142/what-does-it-mean-to-escape-a-string

    $name     = $this->security->xss_clean($name_escape);
    $username = $this->security->xss_clean($username_escape);
    $email    = $this->security->xss_clean($email_escape);
    $password = $this->input->post('password');
    // XSS Clean = Anti script alert script alert Club

    $data = array(
      'name'      =>  $name,
      'username'  =>  $username,
      'email'     =>  $password,
      'password'  =>  md5($password)
    );

    if ($doUpload['result'] == 'success') {
      $data['photo']  = $doUpload['file']['file_name'];
    } else {
      print_r($doUpload['error']);
    }

    $doSave = $this->project_m->save_data('users', $data);
    if ($doSave['res'] == 'sukses') {
      redirect(base_url('users'));
    }
  }

  /**
  * Fungsi yg akan menampilkan data dari class ini
  * Fungsi ini akan mem-passing data yg didapat dari model (get_data) ke view
  */
  public function edit($id) {
    // WHERE id = $id
    $where  = ['id' =>  $id];

    // Hanya mengambil 1 row / baris dari data yg ditemukan
    $data['edit']   = $this->project_m->get_data('users', $where)->row();
    $data['title']  = "Edit";

    $this->load->view('layouts/header');
    $this->load->view('users/edit', $data);
    $this->load->view('layouts/footer');
    // Mempassing(mengirim) data users ke view agar bisa ditampilkan
  }

  public function update($id) {
    $path     = './assets/photo/users/';

    // WHERE id = $id
    $where    = ['id' =>  $id];
    // Ambil data untuk mendapatkan value photo
    $resource = $this->project_m->get_data('users', $where)->row();

    // Siapkan data yg akan diupdate
    $data = array(
      'name'      =>  $this->input->post('name'),
      'username'  =>  $this->input->post('username'),
      'email'     =>  $this->input->post('email'),
      'password'  =>  md5($this->input->post('password'))
    );

    // Jika ada file yg diinput
    if ($_FILES['photo']['error'] < 1) {
      $doUpload = $this->project_m->upload_foto('photo', $path);

      if ($doUpload['result'] == 'success') {
        // Hapus foto sebelumnya
        unlink($path.$resource->photo);
        $data['photo']  = $doUpload['file']['file_name'];
      } else {
        echo $doUpload['error'];
      }

      $doUpdate = $this->project_m->update_data('users', $where, $data);
      if ($doUpdate['res'] == 'sukses') {
        redirect(base_url('users'));
      }
    }
  }

  /**
  * Fungsi yg akan menghapus data dari class ini
  */
  public function delete($id) {
    $path     = './assets/photo/users/';

    // WHERE id = $id
    $where    = ['id' =>  $id];
    // Ambil data untuk mendapatkan value photo
    $resource = $this->project_m->get_data('users', $where)->row();
    if ($resource) {
      // Hapus foto dari .assets/photo/users/namafoto.jpg
      unlink($path.$resource->photo);
      $doDelete = $this->project_m->delete_data('users', $where);
      if ($doDelete['res'] == 'sukses') {
        redirect(base_url('users'));
      }
    }
  }

}
