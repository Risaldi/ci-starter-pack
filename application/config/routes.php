<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| File ini akan menghubungkan URI Request (Yg kalian ketik di address bar pada bwroser) ke fungsi yg ada di controller.
|
| Contoh:
|	example.com/class/method/id/

| Untuk lebih lanjut, kalian bisa cek dokumentasinya di:
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| ROUTES Yang Sudah Disediakan
| -------------------------------------------------------------------------
|
| Ada beberapa Rute yg sudah disediakan oleh codeigniter diantaranya:
|
|	$route['default_controller'] = 'welcome';
|
| Route ini yg menjadi default jika URI tidak mengarah ke class atau method.
|
|	$route['404_override'] = 'errors/page_missing';
|
| Route ini akan otomatis dijalankan jika URI Request tidak ditemukan pada aplikasi.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| NOTE: Gw belum terlalu ngerti untuk bagian yg ini, jadi bisa cari sendiri maksudnya apa. jangan lupa share jika
| Kalian tau apa maksudnya.
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
| WARNING: Lanjut ke models/Project_M.php
*/
$route['default_controller'] = 'welcome';
$route['user']  = 'users/index';
// Saat kalian ke localhost/project/user
// Route akan mengarahkan ke controller Users dan Fungsi index
$route['user/tambah']  = 'users/create';
// Saat kalian ke localhost/project/user/create
// Route akan mengarahkan ke controller Users dan Fungsi create
$route['user/edit/(:num)']  = 'users/edit/$1';
// Saat kalian ke localhost/project/user/edit/1
// Route akan mengarahkan ke controller Users, Fungsi edit dan menerima parameter id
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
