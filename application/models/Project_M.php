<?php
/*
| File ini akan membuat aplikasi kalian bisa berkomunikasi dengan database (Save, Edit, Hapus, dll).
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_M extends CI_Model {

  public function __construct() {
    parent::__construct();
    // $this->load->liblaries('database');
    // NOTE: Kita tidak perlu load liblary database lagi karena sudah dipanggil secara otomatis di autoloader.
  }

  /**
  * Fungsi save, akan menyimpan data ke database
  * @param: Table yg akan dimasukan data (string): $table
  * @param: Data yg akan diinsert (array): $data
  * @return: respon berhasil atau gagal
  */
  public function save_data($table, $data) {
    $doSave = $this->db->insert($table, $data);
    // INSERT INTO $table (data1, data2, data3) VALUES (val1, val2, val3)

    if ($doSave) {
      // Jika berhasil akan me-return array res dengan value sukses
      return ['res' => 'sukses'];
    } else {
      // Jika gagal akan me-return array res dengan value gagal
      return ['res' => 'gagal'];
    }
    // Jika masih bingung bisa tanyakan digrup
  }

  /**
  * Fungsi get, akan memanggil data dari database
  * @param: Table yg akan dimabil datanya (string): $table
  * @param: Data yg akan dipanggil secara spesifik (array): $where [OPTIONAL, jadi jika tidak dipassing datanya dari controller value defaultnya false]
  * @return: Akan me-return data yg dipanggil
  */
  public function get_data($table, $where = FALSE) {
    // Jika where ada atau bernilai TRUE
    // Panggil data dengan menggunakan WHERE
    if($where == TRUE) {
      $doGet  = $this->db->where($where)->get($table);
      // SELECT * FROM $table WHERE $where
      // Return data yg didapat dari query yg dijalankan
      return $doGet;
    }

    $doGet  = $this->db->get($table);
    // SELECT * FROM $table
    // Return data yg didapat dari query yg dijalankan
    return $doGet;

  }

  /**
  * Fungsi update, akan mengubah data dari database
  * @param: Table yg akan dimabil datanya (string): $table
  * @param: Data yg akan dipanggil secara spesifik (array): $where [WAJIB]
  * @param: Data baru yg akan disimpan (array): $set [WAJIB]
  * @return: respon berhasil atau gagal
  */
  public function update_data($table, $where, $set) {
    $doUpdate = $this->db->where($where)
                         ->set($set)
                         ->update($table);
    // UPDATE $table SET dat1=val1, data2=val2 WHERE $where
    if ($doUpdate) {
      // Jika berhasil akan me-return array res dengan value sukses
      return ['res' => 'sukses'];
    } else {
      // Jika gagal akan me-return array res dengan value gagal
      return ['res' => 'gagal'];
    }
    // Jika masih bingung bisa tanyakan digrup

  }

  /**
  * Fungsi delete, akan mengahpus data dari database
  * @param: Table yg akan dihapus datanya (string): $table
  * @param: Data mana yg akan dihapus (array): $where [WAJIB]
  * @return: respon berhasil atau gagal
  */
  public function delete_data($table, $where) {
    $doDelete = $this->db->where($where)
                         ->delete($table);
    // DELETE FROM $table WHERE $where
    if ($doDelete) {
      // Jika berhasil akan me-return array res dengan value sukses
      return ['res' => 'sukses'];
    } else {
      // Jika gagal akan me-return array res dengan value gagal
      return ['res' => 'gagal'];
    }
  }

  /**
  * Fungsi upload foto, akan mengupload foto ke direktori aplikasi
  * @param: Name dari form input yg digunakan (string): $name [WAJIB]
  * @param: Lokasi untuk menyimpan foto (string): $path [WAJIB]
  * @return: respon berhasil atau gagal dan data data dari foto yg diinput
  */
  public function upload_foto($name, $path) {
    $config['upload_path']    = $path;
    $config['allowed_types']  = 'jpg|jpeg|png|ico'; // Tipe file yg diizinkan untuk diupload
    $config['max_size']       = 1024*5; // Maksimal ukuran foto yg bisa diinput
    $config['encrypt_name']   = FALSE; // Mengenkripsi nama foto

    // Load liblary upload
    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    // Upload foto
    if($this->upload->do_upload($name)) {
        $return = array(
                        'result' => 'success',
                        'file' => $this->upload->data(),
                        'error' => ''
                      );
        return $return;
    } else {
        $return = array(
                        'result' => 'failed',
                        'file' => '',
                        'error' => $this->upload->display_errors()
                      );
        return $return;
    }
  }

  // WARNING: Lanjut ke controllers/Users.php

}
