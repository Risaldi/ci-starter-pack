<div class="container-fluid">
  <div class="row my-3">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-primary py-1">
          <h5 class="card-title py-2 mb-0 text-white"><?php echo $title ?> Data User</h5>
        </div>
        <div class="card-body">
          <?php
            echo validation_errors();
            $attributes = array('enctype' => 'multipart/form-data');
          ?>
          <?php echo form_open('/users/create', $attributes); ?>
            <?php $this->load->view('users/form') ?>
          <?php echo form_close() ?>
        </div>
      </div>
    </div>
  </div>
</div>
