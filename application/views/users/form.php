<div class="row">
  <div class="col-12 col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Name</label>
      <input type="text" name="name" class="form-control" value="<?php echo (isset($edit)) ? $edit->name : '' ?>">
    </div>
    <div class="form-group">
      <label>Username</label>
      <input type="text" name="username" class="form-control" value="<?php echo (isset($edit)) ? $edit->username : '' ?>">
    </div>
    <div class="form-group">
      <label>Email</label>
      <input type="text" name="email" class="form-control" value="<?php echo (isset($edit)) ? $edit->email : '' ?>">
    </div>
  </div>
  <div class="col-12 col-sm-12 col-md-6 col-lg-6">
    <div class="form-group">
      <label>Password</label>
      <input type="password" name="password" class="form-control">
    </div>
    <div class="form-group">
      <label>Photo</label>
      <input type="file" name="photo" class="form-control py-1">
    </div>
    <?php if (isset($edit)): ?>
      <div class="form-group">
        <img src="<?php echo base_url('assets/photo/users/'.$edit->photo) ?>" alt="" class="img-fluid">
      </div>
    <?php endif; ?>
    <div class="form-group">
      <button type="submit" class="btn btn-primary">Submit</button>
      <a href="<?php echo base_url('user') ?>" class="btn btn-danger">Kembali</a>
    </div>
  </div>
</div>
