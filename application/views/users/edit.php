<div class="container-fluid">
  <div class="row my-3">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-primary py-1">
          <h5 class="card-title py-2 mb-0 text-white"><?php echo $title ?> Data User</h5>
        </div>
        <div class="card-body">
          <form action="<?php echo base_url('users/update/'.$edit->id) ?>" method="post" enctype="multipart/form-data" class="form">
            <?php $this->load->view('users/form') ?>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
