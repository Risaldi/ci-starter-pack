<div class="container-fluid">
  <div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 offset-md-3 my-3">
      <div class="card">
        <div class="card-header bg-primary">
          <h3 class="card-title mb-0 py-1 text-white">Login!</h3>
        </div>
        <div class="card-body">
          <form class="form" action="<?php echo base_url('login/doLogin') ?>" method="post">
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" required>
            </div>
            <div class="form-group">
              <button class="btn btn-primary btn-block">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
